# -*- coding: utf-8 -*-

from resources.lib import kodiutils
from resources.lib import kodilogging
import logging
import time
import xbmc
import xbmcaddon
import xbmcgui
import urllib2
import json


ADDON = xbmcaddon.Addon()
logger = logging.getLogger(ADDON.getAddonInfo('id'))
addonname = "VPN Watchdog"
monitor = xbmc.Monitor()

def fetch_ip_info(url, source_country_code):
  request = urllib2.Request(url)
  response = urllib2.urlopen(request)

  if response.getcode() != 200:
    raise Exception("Failed to get IP info")

  response = response.read()

  ip_info = json.loads(response)

  logger.debug(response)

  if ip_info["countryCode"].lower() == source_country_code.lower():
    return 0

  return 1

def stop_player():
  player = xbmc.Player()

  if player.isPlaying():
    player.pause()

    stop_player = xbmcgui.Dialog().yesno(addonname, "VPN seems to be disconected!", "Would you like to stop the player?")

    if stop_player:
      logger.info("Stopping player")
      kodiutils.notification(addonname, "Stopping player", time=1000)
      
      player.stop()

def watchdog():
  ip_info_api = "http://ip-api.com/json"
  source_country_code = kodiutils.get_setting("source_country_code")
  watchdog_interval = kodiutils.get_setting_as_int("watchdog_interval")

  while not monitor.abortRequested():
    try:
      if not fetch_ip_info(ip_info_api, source_country_code):
        logger.info("VPN disconnected")
        
        stop_player()
    except Exception, e:
      logger.error(e.message)
      kodiutils.notification(addonname, e.message, time=1000)

      stop_player()

    # Sleep/wait for abort for some interval
    if monitor.waitForAbort(watchdog_interval) and monitor.abortRequested():
      # Abort was requested while waiting. We should exit
      logger.info("Abort requested")
      
      break

def run():
  # Watch changes to IP
  watchdog()